<div class="container">
    <form method="POST" action="?controller=tasks&action=create_task" enctype="multipart/form-data">
        <div class="form-group">
            <label for="example-text-input">Name</label>
            <input id="name" class="form-control" type="text" name='name' placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input id="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name='email'>
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleTextarea">Task content</label>
            <textarea id="content" class="form-control" id="exampleTextarea" rows="3" name='content'></textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input id="picture" type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" name="picture">
            <small id="fileHelp" class="form-text text-muted">Please, choose a picture to make your task looks better</small>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitleUserName"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <img id="modalPicture" src="" alt="modalImage" style="width: 320px; height: 240px;">
                <br>
                <h4>Email: </h4><h3 id="modalEmail"></h3>
                <br>
                <h4>Content: </h4><p id="modalContent"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<span><button onclick="preview()" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Show modal</button></span>
<script>
    var preview = function () {
        var name = document.getElementById('name').value;
        var email = document.getElementById('email').value;
        var content = document.getElementById('content').value;
        var picture = document.getElementById('picture').files[0];

        var reader = new FileReader();

        reader.onload = function(e) {
            $('#modalPicture').attr('src', e.target.result);
        }

        var imageToShow = reader.readAsDataURL(picture);

        document.getElementById('modalTitleUserName').innerHTML = name;
        document.getElementById('modalEmail').innerHTML = email;
        document.getElementById('modalContent').innerHTML = content;
        var img = document.createElement('img');


    }
</script>