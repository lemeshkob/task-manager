<div class="container-fluid" style="margin-top: 0.5em;">
    <div class="container-fluid" style="background-color: #efefef;">
        <?php echo '<img class="card-img-top mx-auto d-block" style="width: 480px; height: 360px;"  src="data:image/jpeg;base64,'.base64_encode( $task->picture ).'"/>'; ?>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <h2>
                    <?php echo $task->user_name; ?>
                </h2>
                <?php if (!$task->state) { ?>
                    <h6 class="card-text" style="color: #a3002b;">Status: Not finished yet</h6>
                <?php } else { ?>
                    <h6 class="card-text" style="color: #83f442;">Status:Done</h6>
                <?php } ?>

                <?php if (isset($_SESSION['role']) && $_SESSION['role'] == User::ROLE_ADMIN) { ?>
                    <a href='?controller=tasks&action=complete_task&id=<?php echo $task->id; ?>' class="btn btn-success <?php if ($task->state){echo 'disabled';} ?>" style="margin-right: 0.5em;">Complete</a>
                    <a href='?controller=tasks&action=edit_task&id=<?php echo $task->id; ?>' class="btn btn-success">Edit</a>
                <?php }?>

                <h6 style="color: #7a7a7a;">
                    <?php echo $task->email; ?>
                </h6>
                <p>
                    <?php echo $task->content; ?>
                </p>
            </div>
        </div>
    </div>
</div>
