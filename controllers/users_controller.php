<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/15/18
 * Time: 2:49 PM
 */

class UsersController
{
    public function register() {
        require_once ('views/users/register.php');

        if (isset($_POST['name'])
            && isset($_POST['email'])
            && isset($_POST['password'])
            && isset($_POST['password_confirm'])) {

            if ($_POST['password'] === $_POST['password_confirm']) {
                $name = $_POST['name'];
                $email = $_POST['email'];
                $password = sha1($_POST['password']);
                User::register($name, $email, $password);
            }

        }
    }

    public function login() {

        session_destroy();

        ob_start();
        session_start();

        require_once ('views/users/login.php');

        if (isset($_POST['email']) && isset($_POST['password'])) {
            $email = $_POST['email'];
            $password = sha1($_POST['password']);

            $user = User::login($email, $password);

            if (isset($user->name)) {
                $_SESSION['user_name'] = $user->name;
                $_SESSION['email'] = $user->email;
                $_SESSION['role'] = $user->role;

                header('Refresh: 1; URL = ?controller=tasks&action=index');
            } else {
                echo "<h3 style='color: indianred;'>Wrong email or/and password! <br> Please, doublecheck</h3>";
            }


        }
    }

    public function logout() {
        unset($_SESSION['user_name']);
        unset($_SESSION['email']);
        unset($_SESSION['role']);
        header('Refresh: 1; URL = ?controller=tasks&action=index');
    }
}