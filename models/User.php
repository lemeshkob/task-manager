<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/15/18
 * Time: 2:37 PM
 */

class User
{
    public $id;
    public $name;
    public $email;
    public $password;
    public $role;

    const ROLE_USER = 'USER';
    const ROLE_ADMIN = 'ADMIN';

    public function __construct($id, $name, $email, $password, $role){
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
    }

    public static function register($name, $email, $password) {
        $password = sha1($password);
        $db = Db::getInstance();
        $req = $db->query(
            "INSERT INTO users (name, email, password) VALUES ('$name', '$email','$password')"
        );
    }

    public static function login($email, $password) {
        $password = sha1($password);

        $db = Db::getInstance();
        $req = $db->query("SELECT * FROM users WHERE email='$email' AND password='$password'");
        $req->execute();
        $user = $req->fetch();

        return new User($user['id'], $user['name'], $user['email'], $user['password'], $user['role']);
    }

}